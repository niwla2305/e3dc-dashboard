# E3DC Dashboard
This is an **unofficial** Dashboard for E3DC Storages
## Installation
### Setup your E3DC Storage.
* Go to personalisation > user profile
* set the RSCP password and the RSCP user password

### Configure the Software
* Copy settings.json.example to settings.json
* Change the values in settings.json. Key is the RSCP password and password the RSCP user password. ip is the IP of your storage, I reccommend setting a static IP
* 

### Deploy with Docker
I dont´t know if this works also on other platforms, its tested on Linux
* Make sure you have docker installed and running
* go inside the root directory of the project and execute `docker build . -t e3dc` (If it fails, try as root)
* `docker volume create e3dc_data`
* `docker run -p 5050:80 -v e3dc_data:/usr/src/app/e3dc_dashboard/data --restart=unless-stopped e3dc`
