from e3dc import *

TCP_IP = '192.168.178.52'
USERNAME = 'local.user'
PASS = '123456'
KEY = '123456'

print("local connection")
e3dc = E3DC(E3DC.CONNECT_LOCAL, username=USERNAME, password=PASS, ipAddress=TCP_IP, key=KEY)
# The following connections are performed through the RSCP interface
print(e3dc.poll())
