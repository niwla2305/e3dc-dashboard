# This file is a template, and might need editing before it works on your project.
FROM python:3.6

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -U https://github.com/fsantini/python-e3dc/archive/python3.zip

COPY . /usr/src/app

EXPOSE 80

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

CMD gunicorn --bind 0.0.0.0:80 wsgi

