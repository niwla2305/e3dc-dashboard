import json


def getconfig(mode):
    with open("settings.json", mode) as config_file:
        config_raw = config_file.read()
        configuration = json.loads(config_raw)
        return configuration
