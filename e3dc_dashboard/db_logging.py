import time
from datetime import datetime
from threading import *
import os
from e3dc import E3DC

from e3dc_dashboard import _helpers, models


def tell_editor_to_not_remove():
    pass


def log():
    print("Log loop")
    KEY = os.environ.get("e3dc_key") or _helpers.getconfig("r")["key"]
    TCP_IP = os.environ.get("e3dc_ip") or _helpers.getconfig("r")["ip"]
    USERNAME = os.environ.get("e3dc_user") or _helpers.getconfig("r")["user"]
    PASS = os.environ.get("e3dc_password") or _helpers.getconfig("r")["password"]

    e3dc = E3DC(E3DC.CONNECT_LOCAL, username=USERNAME, password=PASS, ipAddress=TCP_IP, key=KEY)
    while True:
        data = e3dc.poll()
        models.Log.create(autarky=data["autarky"], battery=data["consumption"]["battery"],
                          house=data["consumption"]["house"], wallbox=data["consumption"]["wallbox"],
                          grid=data["production"]["grid"], solar=data["production"]["solar"],
                          selfConsumption=data["selfConsumption"],
                          stateOfCharge=data["stateOfCharge"],
                          sysStatus=data["sysStatus"], time=datetime.now())
        time.sleep(300)


t = Thread(target=log).start()
