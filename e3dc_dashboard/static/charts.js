window.solar = [];
var grid = [];
var house = [];
var battery = [];
var stateOfCharge = [];
window.historicdata = {};

if (window.innerWidth > 575) {
  document.getElementById("daychart").height = "200";
}

var ctx = document.getElementById("daychart").getContext("2d");
var DayChart = new Chart(ctx, {
  type: "line",
  data: {
    labels: [
      "0:00",
      "1:00",
      "2:00",
      "3:00",
      "4:00",
      "5:00",
      "6:00",
      "7:00",
      "8:00",
      "9:00"
    ],
    datasets: [
      {
        yAxisID: "default",
        data: [],
        label: "Produktion",
        borderColor: "#28A745",
        fill: false,
        pointRadius: 3
      },
      {
        data: [],
        label: "Verbrauch",
        borderColor: "#007BFF",
        fill: false,
        pointRadius: 3
      },
      {
        data: [],
        label: "Akkuladung",
        borderColor: "#3cba9f",
        fill: false,
        pointRadius: 3
      },
      {
        data: [],
        label: "Auspeisung / Einspeisung",
        borderColor: "#DC3545",
        fill: false,
        pointRadius: 3
      },
      {
        data: [],
        yAxisID: "percent",
        label: "Akkustand in %",
        borderColor: "#000000",
        fill: false,
        pointRadius: 3
      }
    ]
  },
  options: {
    pointRadius: 0,
    title: {
      display: true,
      text: "Heute (01.02.2020)"
    },
    scales: {
      yAxes: [
        {
          type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
          display: true,
          position: "left",
          id: "default"
        },
        {
          type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
          display: true,
          position: "right",
          id: "percent",

          // grid line settings
          gridLines: {
            drawOnChartArea: false // only want the grid lines for one axis to show up
          }
        }
      ]
    },
    plugins: {
      zoom: {
        // Container for pan options
        pan: {
          // Boolean to enable panning
          enabled: true,

          // Panning directions. Remove the appropriate direction to disable
          // Eg. 'y' would only allow panning in the y direction
          mode: "xy"
        },

        // Container for zoom options
        zoom: {
          // Boolean to enable zooming
          enabled: true,

          // Zooming directions. Remove the appropriate direction to disable
          // Eg. 'y' would only allow zooming in the y direction
          mode: "xy"
        }
      }
    }
  }
});

DayHistoryDay = new Date()

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

Date.prototype.removeDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() - days);
  return date;
}

function convertDate(date) {
  return date.toISOString().substr(0, 10);
}

function loadChartDay(date) {
  if (date === undefined) {
    var date = "today"
  } else {
    var date = convertDate(date)
  }
  var jqXHR = $.get(`/api/history/day/${date}/`, function(data, status) {
    window.historicdata = data;
    DayChart.data.datasets[0].data = data["solar"];
    DayChart.data.datasets[1].data = data["house"];
    DayChart.data.datasets[2].data = data["battery"];
    DayChart.data.datasets[3].data = data["grid"];
    DayChart.data.datasets[4].data = data["stateOfCharge"];
    DayChart.data.labels = data["time"];
    DayChart.options.title.text = date;
    DayChart.update();
    DayChart.resetZoom()
    if (date === "today" || date === convertDate(new Date())) {
      $('#day_today').addClass("active")
      $('#day_next').addClass("disabled")
    } else {
      $('#day_today').removeClass("active")
      $('#day_next').removeClass("disabled")
    }
  });
}

function updateCharts() {
  loadChartDay();
}

window.setInterval(updateCharts, "20000");
updateCharts();

function daily_history_before() {
  DayHistoryDay = DayHistoryDay.removeDays(1)
  loadChartDay(DayHistoryDay)
}

function daily_history_next() {
  DayHistoryDay = DayHistoryDay.addDays(1)
  loadChartDay(DayHistoryDay)
}

function daily_history_today() {
  loadChartDay()
}
