
function update_all_kiosk() {
    var jqXHR = $.get("/api/now/poll/", function (data, status) {
        document.getElementById("prod_value").innerHTML = data["production"]["solar"] + "W"
        document.getElementById("consumption_value").innerHTML = data["consumption"]["house"] + "W"
        document.getElementById("accu_value").innerHTML = data["consumption"]["battery"] + "W"
        document.getElementById("grid_value").innerHTML = data["production"]["grid"] + "W"
        document.getElementById("accustate_val").innerHTML = data["stateOfCharge"] + "%"
    });
}

function openFullscreen() {
  var elem = document.documentElement;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    /* IE/Edge */
    elem.msRequestFullscreen();
  }
}


update_all_kiosk()
window.setInterval(update_all_kiosk, "5000")