window.onload = () => {
  "use strict";

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js");
  }
};

function update_all() {
    var jqXHR = $.get("/api/now/poll/", function (data, status) {
        document.getElementById("accustate").innerHTML = "Akkuladung: " + data["stateOfCharge"] + "%"
        document.getElementById("accustate").style.width = data["stateOfCharge"] + "%"

        document.getElementById("autarky").innerHTML = "Autarkie: " + data["autarky"] + "%"
        document.getElementById("autarky").style.width = data["autarky"] + "%"

        document.getElementById("selfConsumption").innerHTML = "Eigenstrom: " + data["selfConsumption"] + "%"
        document.getElementById("selfConsumption").style.width = data["selfConsumption"] + "%"

        document.getElementById("solar").innerHTML = data["production"]["solar"] + " Watt"
        document.getElementById("consumption").innerHTML = data["consumption"]["house"] + " Watt"
        document.getElementById("accu").innerHTML = data["consumption"]["battery"] + " Watt"
        document.getElementById("grid").innerHTML = data["production"]["grid"] + " Watt"
    });
}

update_all()
window.setInterval(update_all, "5000")