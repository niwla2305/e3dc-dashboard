import datetime
import dateutil.parser
from e3dc import *
from flask import *
from flask_jsonpify import jsonpify

from e3dc_dashboard import _helpers
from e3dc_dashboard import app
from e3dc_dashboard import models, db_logging
import os

db_logging.tell_editor_to_not_remove()
# read some config
KEY = os.environ.get("e3dc_key") or _helpers.getconfig("r")["key"]
TCP_IP = os.environ.get("e3dc_ip") or _helpers.getconfig("r")["ip"]
USERNAME = os.environ.get("e3dc_user") or _helpers.getconfig("r")["user"]
PASS = os.environ.get("e3dc_password") or _helpers.getconfig("r")["password"]
e3dc = E3DC(E3DC.CONNECT_LOCAL, username=USERNAME, password=PASS, ipAddress=TCP_IP, key=KEY)


@app.route('/api/now/poll/')
def api_now_poll():
    return jsonpify(e3dc.poll())


@app.route('/api/history/day/today/')
def api_history_today_poll():
    returnvalues = {
        "autarky": [],
        "battery": [],
        "house": [],
        "wallbox": [],
        "grid": [],
        "solar": [],
        "selfConsumption": [],
        "stateOfCharge": [],
        "sysStatus": [],
        "time": []
    }
    for i in models.Log.select():
        if i.time.date() == datetime.date.today():
            returnvalues["autarky"].append(i.autarky)
            returnvalues["battery"].append(i.battery)
            returnvalues["house"].append(i.house)
            returnvalues["wallbox"].append(i.wallbox)
            returnvalues["grid"].append(i.grid)
            returnvalues["solar"].append(i.solar)
            returnvalues["selfConsumption"].append(i.selfConsumption)
            returnvalues["stateOfCharge"].append(i.stateOfCharge)
            returnvalues["sysStatus"].append(i.sysStatus)
            returnvalues["time"].append(str(i.time.hour) + ":" + str(i.time.minute))
    return jsonpify(returnvalues)


@app.route('/api/history/day/<day>/')
def api_history_day_poll(day):
    returnvalues = {
        "autarky": [],
        "battery": [],
        "house": [],
        "wallbox": [],
        "grid": [],
        "solar": [],
        "selfConsumption": [],
        "stateOfCharge": [],
        "sysStatus": [],
        "time": []
    }
    for i in models.Log.select():
        if i.time.date() == dateutil.parser.parse(day).date():
            returnvalues["autarky"].append(i.autarky)
            returnvalues["battery"].append(i.battery)
            returnvalues["house"].append(i.house)
            returnvalues["wallbox"].append(i.wallbox)
            returnvalues["grid"].append(i.grid)
            returnvalues["solar"].append(i.solar)
            returnvalues["selfConsumption"].append(i.selfConsumption)
            returnvalues["stateOfCharge"].append(i.stateOfCharge)
            returnvalues["sysStatus"].append(i.sysStatus)
            returnvalues["time"].append(str(i.time.hour) + ":" + str(i.time.minute))
    return jsonpify(returnvalues)


@app.route('/api/history/week/this/')
def api_history_week_poll():
    returnvalues = {
        "monday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "tuesday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "wednesday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "thursday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "friday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "saturday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
        "sunday": {
            "autarky": [],
            "battery": [],
            "house": [],
            "wallbox": [],
            "grid": [],
            "solar": [],
            "selfConsumption": [],
            "stateOfCharge": [],
            "sysStatus": [],
            "time": []},
    }

    weekdayandnumber = {
        "monday": 1,
        "tuesday": 2,
        "wednesday": 3,
        "thursday": 4,
        "friday": 5,
        "saturday": 6,
        "sunday": 7,
    }

    for i in models.Log.select():
        if i.time.date().isocalendar()[1] == datetime.date.today().isocalendar()[1]:
            print(i.time.date().isoweekday())
            for key, value in weekdayandnumber.items():
                print(key)
                if i.time.date().isoweekday() == value:
                    print("Here we go")
                    returnvalues[key]["battery"].append(i.battery)
                    returnvalues[key]["house"].append(i.house)
                    returnvalues[key]["wallbox"].append(i.wallbox)
                    returnvalues[key]["grid"].append(i.grid)
                    returnvalues[key]["solar"].append(i.solar)

    for key, value in returnvalues.items():
        for key2, value2 in returnvalues[key].items():
            returnvalues[key][key2] = sum(returnvalues[key][key2])

    return jsonpify(returnvalues)


@app.route('/')
def dashboard():
    return render_template("index.html")


@app.route('/kiosk/')
def kiosk():
    return render_template("kiosk.html")


@app.route('/sw.js')
def service_worker():
    response = make_response(send_from_directory('static', 'sw.js'))
    response.headers['Cache-Control'] = 'no-cache'
    return response
