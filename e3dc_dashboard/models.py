from peewee import *

db = SqliteDatabase('data/e3dc.db')


class Log(Model):
    autarky = FloatField()
    battery = FloatField()
    house = FloatField()
    wallbox = FloatField()
    grid = FloatField()
    solar = FloatField()
    selfConsumption = FloatField()
    stateOfCharge = FloatField()
    sysStatus = FloatField()
    time = DateTimeField()

    class Meta:
        database = db


db.create_tables([Log])
